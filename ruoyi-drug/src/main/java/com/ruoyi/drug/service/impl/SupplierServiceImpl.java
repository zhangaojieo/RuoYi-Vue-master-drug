package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Supplier;
import com.ruoyi.drug.mapper.SupplierMapper;
import com.ruoyi.drug.service.ISupplierService;
import org.springframework.stereotype.Service;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 18:06
 */

@Service
public class SupplierServiceImpl extends ServiceImpl<SupplierMapper, Supplier> implements ISupplierService {
}
