package com.ruoyi.drug.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Shops;
import com.ruoyi.drug.mapper.ShopsMapper;
import com.ruoyi.drug.service.IShopsService;
import org.springframework.stereotype.Service;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 19:32
 */
@Service
public class ShopsServiceImpl extends ServiceImpl<ShopsMapper, Shops> implements IShopsService {
}
