package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Drug;
import com.ruoyi.drug.mapper.DrugMapper;
import com.ruoyi.drug.service.IDrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: ZhangAJ
 * @create: 2023年08月02日 14:04
 */
@Service
public class DrugServiceImpl extends ServiceImpl<DrugMapper, Drug> implements IDrugService {

    @Autowired
    private DrugMapper drugMapper;
    @Override
    public List<Drug> list(Drug drug) {
        return drugMapper.list(drug);
    }

}
