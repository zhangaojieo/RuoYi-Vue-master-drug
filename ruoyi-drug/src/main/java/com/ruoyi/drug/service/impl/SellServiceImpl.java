package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.drug.domain.Drug;
import com.ruoyi.drug.domain.Sell;
import com.ruoyi.drug.mapper.SellMapper;
import com.ruoyi.drug.service.ISellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: ZhangAJ
 * @create: 2023年08月03日 1:30
 */
@Service
public class SellServiceImpl extends ServiceImpl<SellMapper, Sell> implements ISellService {

    @Autowired
    private SellMapper sellMapper;
    @Autowired
    private DrugServiceImpl drugService;

    @Override
    public List<Sell> list(Sell sell) {
        return sellMapper.list(sell);
    }
    @Transactional
    @Override
    public boolean save(Sell sell) {
        //获得药品库存数量,页面输入的数量要和库存数量就行比较,如果小于,正常操作,如果大于库存数量,提示库存不足
        //同时还要将销售数据添加到sell表中
        Drug drug = drugService.getById(sell.getDrugid());
        if(drug.getCount()<Long.valueOf(sell.getSellquantity())){
            return false;
        }
        drug.setCount(drug.getCount()-Long.valueOf(sell.getSellquantity())); //获得药品库存数量
        sell.setAmount(String.valueOf(Integer.valueOf(drug.getPrice())*Integer.valueOf(sell.getSellquantity())));
        drugService.updateById(drug); //重新更新库存
        sell.setSelltime(DateUtils.getTime());
        sell.setSellnum(UUID.randomUUID().toString());
        sell.setCreatetime(DateUtils.getTime());
        return super.save(sell); //该数据表是给销售表添加记录
    }
}
