package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Store;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 16:24
 */
public interface IStoreService extends IService<Store> {
}
