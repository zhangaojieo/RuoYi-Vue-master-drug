package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Brand;
import com.ruoyi.drug.mapper.BrandMapper;
import com.ruoyi.drug.service.IBrandService;
import com.ruoyi.drug.service.ICategoryService;
import org.springframework.stereotype.Service;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 15:13
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {
}
