package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Shops;

public interface IShopsService extends IService<Shops> {
}
