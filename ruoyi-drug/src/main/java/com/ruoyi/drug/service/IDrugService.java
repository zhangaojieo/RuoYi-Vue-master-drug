package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Drug;

import java.util.List;

public interface IDrugService extends IService<Drug> {

    List<Drug> list(Drug drug);

}
