package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Supplier;

public interface ISupplierService extends IService<Supplier> {


}
