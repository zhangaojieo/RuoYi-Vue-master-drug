package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Store;
import com.ruoyi.drug.mapper.StoreMapper;
import com.ruoyi.drug.service.IStoreService;
import org.springframework.stereotype.Service;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 16:25
 */
@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {
}
