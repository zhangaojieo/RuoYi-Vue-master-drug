package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Category;

public interface ICategoryService extends IService<Category> {
}
