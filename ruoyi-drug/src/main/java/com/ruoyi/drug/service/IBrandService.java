package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Brand;

public interface IBrandService extends IService<Brand> {
}
