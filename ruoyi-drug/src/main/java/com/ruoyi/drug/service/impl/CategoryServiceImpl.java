package com.ruoyi.drug.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.drug.domain.Category;
import com.ruoyi.drug.mapper.CategoryMapper;
import com.ruoyi.drug.service.ICategoryService;
import org.springframework.stereotype.Service;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 11:26
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements  ICategoryService {
}
