package com.ruoyi.drug.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.drug.domain.Sell;

import java.util.List;

public interface ISellService extends IService<Sell> {

    List<Sell> list(Sell sell);

    boolean save(Sell sell);
}
