package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: ZhangAJ
 * @create: 2023年08月03日 1:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sell implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    private Long sellid;

    /** 药品名称 */
    @Excel(name = "药品名称")
    private Long drugid;
    @TableField(exist = false)
    private String drugname;

    /** 销售编号 */
    @Excel(name = "销售编号")
    private String sellnum;

    /** 销售时间 */
    @Excel(name = "销售时间")
    private String selltime;

    /** 销售数量 */
    @Excel(name = "销售数量")
    private String sellquantity;

    /** 操作人 */
    @Excel(name = "操作人")
    private String opename;

    /** 金额 */
    @Excel(name = "金额")
    private String amount;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 删除标记 */
    @TableLogic
    private Integer deleted;
    private String remark;

    /** 门店名称 */
    @Excel(name = "门店名称")
    private Long sid;
    @TableField(exist = false)
    private String sname;

}
