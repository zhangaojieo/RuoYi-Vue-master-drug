package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 药品分类对象 category
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category implements Serializable
{
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO) //标注该属性是表的主键
    private Long cid;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String cname;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 删除标记 */
    @TableLogic //该注解标签是删除标记
    private Integer deleted;
}
