package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: ZhangAJ
 * @create: 2023年08月02日 10:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Drug implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long drugid;
    /** 单价 */
    @Excel(name = "单价")
    private String price;

    /** 供应商 */
    @Excel(name = "供应商")
    private Long supid;
    @TableField(exist = false)
    private String supname;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private Long storid;
    @TableField(exist = false)
    private String storename;

    /** 所属类别 */
    @Excel(name = "所属类别")
    private Long cid;
    @TableField(exist = false)
    private String cname;

    /** 品牌 */
    @Excel(name = "品牌")
    private Long brid;
    @TableField(exist = false)
    private String brname;

    /** 批准号 */
    @Excel(name = "批准号")
    private String drugnum;

    /** 药品名称 */
    @Excel(name = "药品名称")
    private String drugname;

    /** 生产日期 */
    @Excel(name = "生产日期")
    private String datemanufacture;

    /** 到期日期 */
    @Excel(name = "到期日期")
    private String duedate;

    /** 计量 */
    @Excel(name = "计量")
    private String drugdosage;

    /** 使用方式 */
    @Excel(name = "使用方式")
    private String illustrate;

    /** 包装类型 */
    @Excel(name = "包装类型")
    private String size;

    /** 是否处方 */
    @Excel(name = "是否处方")
    private String prescription;

    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String manufacturer;

    /** 使用人群 */
    @Excel(name = "使用人群")
    private String usergroup;

    /** 存储方式 */
    @Excel(name = "存储方式")
    private String storeconditions;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long count;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 删除标记 */
    @TableLogic
    private Integer deleted;
    private String remark;
}
