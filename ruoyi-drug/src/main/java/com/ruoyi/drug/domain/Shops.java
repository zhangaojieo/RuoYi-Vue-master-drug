package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: ZhangAJ
 * @create: 2023年08月01日 19:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shops implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long sid;

    /** 门店编号 */
    @Excel(name = "门店编号")
    private String snum;

    /** 门店名称 */
    @Excel(name = "门店名称")
    private String sname;

    /** 所在城市 */
    @Excel(name = "所在城市")
    private String city;

    /** 详情地址 */
    @Excel(name = "详情地址")
    private String address;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String tel;

    /** 管理者 */
    @Excel(name = "管理者")
    private String leader;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;
    @TableLogic

    /** 0未删除 1已删除 */
    @Excel(name = "0未删除 1已删除")
    private Long deleted;
}
