package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 供应商管理对象 supplier
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supplier implements Serializable
{
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long supid;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supname;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 简介 */
    @Excel(name = "简介")
    private String sudesc;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 删除标记 */
    private Long deleted;

}
