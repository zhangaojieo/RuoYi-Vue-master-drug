package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 仓库管理对象 store
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Store implements Serializable
{
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long storid;

    /** 仓库编号 */
    @Excel(name = "仓库编号")
    private String stornum;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String storename;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 删除标记 */
    @TableLogic
    private Long deleted;

    /** 操作人 */
    @Excel(name = "操作人")
    private String director;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

}
