package com.ruoyi.drug.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 品牌管理对象 brand
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Brand implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 品牌id */
    @TableId(type = IdType.AUTO) //标注该属性是表的主键
    private Long brid;

    /** 品牌名称 */

    private String brname;

    /** 品牌简介 */
    @Excel(name = "品牌简介")
    private String bdesc;

    /** 电话 */
    @Excel(name = "电话")
    private String tell;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 地址 */
    @Excel(name = "地址")
    private String adress;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createtime;

    /** 删除标记 */
    @TableLogic //该注解标签是删除标记
    private Integer deleted;


}
