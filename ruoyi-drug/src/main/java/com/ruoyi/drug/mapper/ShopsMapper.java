package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Shops;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopsMapper extends BaseMapper<Shops> {
}
