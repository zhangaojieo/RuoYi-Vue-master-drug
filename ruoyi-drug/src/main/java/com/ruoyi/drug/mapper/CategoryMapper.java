package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryMapper extends BaseMapper<Category> {
}
