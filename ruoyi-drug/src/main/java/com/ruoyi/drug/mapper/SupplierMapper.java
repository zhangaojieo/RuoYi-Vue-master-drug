package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Supplier;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierMapper extends BaseMapper<Supplier> {
}
