package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Drug;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrugMapper extends BaseMapper<Drug> {

    List<Drug> list(Drug drug);
}
