package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Brand;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandMapper extends BaseMapper<Brand> {
}
