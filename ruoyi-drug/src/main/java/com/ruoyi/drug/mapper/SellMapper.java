package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Sell;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: ZhangAJ
 * @create: 2023年08月03日 1:25
 */
@Repository
public interface SellMapper extends BaseMapper<Sell> {

    List<Sell> list(Sell sell);
}
