package com.ruoyi.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.drug.domain.Store;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreMapper extends BaseMapper<Store> {
}
