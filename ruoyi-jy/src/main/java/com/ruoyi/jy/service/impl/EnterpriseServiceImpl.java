package com.ruoyi.jy.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.EnterpriseMapper;
import com.ruoyi.jy.domain.Enterprise;
import com.ruoyi.jy.service.IEnterpriseService;

/**
 * 企业管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Service
public class EnterpriseServiceImpl implements IEnterpriseService
{
    @Autowired
    private EnterpriseMapper enterpriseMapper;

    /**
     * 查询企业管理
     *
     * @param enid 企业管理主键
     * @return 企业管理
     */
    @Override
    public Enterprise selectEnterpriseByEnid(Long enid)
    {
        return enterpriseMapper.selectEnterpriseByEnid(enid);
    }

    /**
     * 查询企业管理列表
     *
     * @param enterprise 企业管理
     * @return 企业管理
     */
    @Override
    public List<Enterprise> selectEnterpriseList(Enterprise enterprise)
    {
        return enterpriseMapper.selectEnterpriseList(enterprise);
    }

    /**
     * 新增企业管理
     *
     * @param enterprise 企业管理
     * @return 结果
     */
    @Override
    public int insertEnterprise(Enterprise enterprise)
    {
        enterprise.setCreatetime(DateUtils.getTime());
        return enterpriseMapper.insertEnterprise(enterprise);
    }

    /**
     * 修改企业管理
     *
     * @param enterprise 企业管理
     * @return 结果
     */
    @Override
    public int updateEnterprise(Enterprise enterprise)
    {
        enterprise.setModifytime(DateUtils.getTime());
        return enterpriseMapper.updateEnterprise(enterprise);
    }

    /**
     * 批量删除企业管理
     *
     * @param enids 需要删除的企业管理主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseByEnids(Long[] enids)
    {
        return enterpriseMapper.deleteEnterpriseByEnids(enids);
    }

    /**
     * 删除企业管理信息
     *
     * @param enid 企业管理主键
     * @return 结果
     */
    @Override
    public int deleteEnterpriseByEnid(Long enid)
    {
        return enterpriseMapper.deleteEnterpriseByEnid(enid);
    }
}
