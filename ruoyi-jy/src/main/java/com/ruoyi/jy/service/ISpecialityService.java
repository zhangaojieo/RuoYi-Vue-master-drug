package com.ruoyi.jy.service;

import java.util.List;
import com.ruoyi.jy.domain.Speciality;

/**
 * 专业管理Service接口
 * 
 * @author 张澳杰
 * @date 2023-07-28
 */
public interface ISpecialityService 
{
    /**
     * 查询专业管理
     * 
     * @param spid 专业管理主键
     * @return 专业管理
     */
    public Speciality selectSpecialityBySpid(Long spid);

    /**
     * 查询专业管理列表
     * 
     * @param speciality 专业管理
     * @return 专业管理集合
     */
    public List<Speciality> selectSpecialityList(Speciality speciality);

    /**
     * 新增专业管理
     * 
     * @param speciality 专业管理
     * @return 结果
     */
    public int insertSpeciality(Speciality speciality);

    /**
     * 修改专业管理
     * 
     * @param speciality 专业管理
     * @return 结果
     */
    public int updateSpeciality(Speciality speciality);

    /**
     * 批量删除专业管理
     * 
     * @param spids 需要删除的专业管理主键集合
     * @return 结果
     */
    public int deleteSpecialityBySpids(Long[] spids);

    /**
     * 删除专业管理信息
     * 
     * @param spid 专业管理主键
     * @return 结果
     */
    public int deleteSpecialityBySpid(Long spid);
}
