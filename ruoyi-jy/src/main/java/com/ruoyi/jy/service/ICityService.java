package com.ruoyi.jy.service;

import java.util.List;
import com.ruoyi.jy.domain.City;

/**
 * 城市管理Service接口
 * 
 * @author 张澳杰
 * @date 2023-07-28
 */
public interface ICityService 
{
    /**
     * 查询城市管理
     * 
     * @param cityid 城市管理主键
     * @return 城市管理
     */
    public City selectCityByCityid(Long cityid);

    /**
     * 查询城市管理列表
     * 
     * @param city 城市管理
     * @return 城市管理集合
     */
    public List<City> selectCityList(City city);

    /**
     * 新增城市管理
     * 
     * @param city 城市管理
     * @return 结果
     */
    public int insertCity(City city);

    /**
     * 修改城市管理
     * 
     * @param city 城市管理
     * @return 结果
     */
    public int updateCity(City city);

    /**
     * 批量删除城市管理
     * 
     * @param cityids 需要删除的城市管理主键集合
     * @return 结果
     */
    public int deleteCityByCityids(Long[] cityids);

    /**
     * 删除城市管理信息
     * 
     * @param cityid 城市管理主键
     * @return 结果
     */
    public int deleteCityByCityid(Long cityid);

    List<City> getAllCityByProviceId(int proid);
}
