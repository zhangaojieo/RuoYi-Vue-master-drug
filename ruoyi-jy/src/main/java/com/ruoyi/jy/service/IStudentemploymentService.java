package com.ruoyi.jy.service;

import java.util.List;

import com.ruoyi.jy.domain.Studentemployment;

/**
 * 就业管理Service接口
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
public interface IStudentemploymentService {
    /**
     * 统计分析不同专业就业人数
     * @return
     */
    List<Studentemployment>getSpeciStuEmpCount();

    /**
     * 统计不同专业的平均薪资
     * @return
     */
    List<Studentemployment>getSpeciAvgMoney();

    /**
     * 统计各个城市的就业人数
     * @return
     */
    List<Studentemployment> getStuEmpCityTotal();
    /**
     * 统计未就业人数
     *
     * @return
     */
    int getStudentEmpUnTotal();

    /**
     * 统计已就业人数
     *
     * @return
     */
    int getStudentEmpTotal();

    /**
     * 统计毕业人数
     *
     * @return
     */
    int getFinishCollegeStuTotal();

    /**
     * 查询就业管理
     *
     * @param sid 就业管理主键
     * @return 就业管理
     */
    public Studentemployment selectStudentemploymentBySid(Long sid);

    /**
     * 查询就业管理列表
     *
     * @param studentemployment 就业管理
     * @return 就业管理集合
     */
    public List<Studentemployment> selectStudentemploymentList(Studentemployment studentemployment);

    /**
     * 新增就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    public int insertStudentemployment(Studentemployment studentemployment);

    /**
     * 修改就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    public int updateStudentemployment(Studentemployment studentemployment);

    /**
     * 批量删除就业管理
     *
     * @param sids 需要删除的就业管理主键集合
     * @return 结果
     */
    public int deleteStudentemploymentBySids(Long[] sids);

    /**
     * 删除就业管理信息
     *
     * @param sid 就业管理主键
     * @return 结果
     */
    public int deleteStudentemploymentBySid(Long sid);
}
