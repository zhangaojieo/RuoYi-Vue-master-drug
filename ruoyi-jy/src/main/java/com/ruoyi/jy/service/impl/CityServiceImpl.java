package com.ruoyi.jy.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.CityMapper;
import com.ruoyi.jy.domain.City;
import com.ruoyi.jy.service.ICityService;

/**
 * 城市管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Service
public class CityServiceImpl implements ICityService
{
    @Autowired
    private CityMapper cityMapper;

    /**
     * 查询城市管理
     *
     * @param cityid 城市管理主键
     * @return 城市管理
     */
    @Override
    public City selectCityByCityid(Long cityid)
    {
        return cityMapper.selectCityByCityid(cityid);
    }

    /**
     * 查询城市管理列表
     *
     * @param city 城市管理
     * @return 城市管理
     */
    @Override
    public List<City> selectCityList(City city)
    {
        return cityMapper.selectCityList(city);
    }

    /**
     * 新增城市管理
     *
     * @param city 城市管理
     * @return 结果
     */
    @Override
    public int insertCity(City city)
    {
        city.setCreatetime(DateUtils.getTime());
        return cityMapper.insertCity(city);
    }

    /**
     * 修改城市管理
     *
     * @param city 城市管理
     * @return 结果
     */
    @Override
    public int updateCity(City city)
    {
        city.setModifytime(DateUtils.getTime());
        return cityMapper.updateCity(city);
    }

    /**
     * 批量删除城市管理
     *
     * @param cityids 需要删除的城市管理主键
     * @return 结果
     */
    @Override
    public int deleteCityByCityids(Long[] cityids)
    {
        return cityMapper.deleteCityByCityids(cityids);
    }

    /**
     * 删除城市管理信息
     *
     * @param cityid 城市管理主键
     * @return 结果
     */
    @Override
    public int deleteCityByCityid(Long cityid)
    {
        return cityMapper.deleteCityByCityid(cityid);
    }

    @Override
    public List<City> getAllCityByProviceId(int proid) {
        return cityMapper.getAllCityByProviceId(proid);
    }
}
