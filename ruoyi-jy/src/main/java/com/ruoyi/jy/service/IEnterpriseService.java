package com.ruoyi.jy.service;

import java.util.List;
import com.ruoyi.jy.domain.Enterprise;

/**
 * 企业管理Service接口
 * 
 * @author 张澳杰
 * @date 2023-07-27
 */
public interface IEnterpriseService 
{
    /**
     * 查询企业管理
     * 
     * @param enid 企业管理主键
     * @return 企业管理
     */
    public Enterprise selectEnterpriseByEnid(Long enid);

    /**
     * 查询企业管理列表
     * 
     * @param enterprise 企业管理
     * @return 企业管理集合
     */
    public List<Enterprise> selectEnterpriseList(Enterprise enterprise);

    /**
     * 新增企业管理
     * 
     * @param enterprise 企业管理
     * @return 结果
     */
    public int insertEnterprise(Enterprise enterprise);

    /**
     * 修改企业管理
     * 
     * @param enterprise 企业管理
     * @return 结果
     */
    public int updateEnterprise(Enterprise enterprise);

    /**
     * 批量删除企业管理
     * 
     * @param enids 需要删除的企业管理主键集合
     * @return 结果
     */
    public int deleteEnterpriseByEnids(Long[] enids);

    /**
     * 删除企业管理信息
     * 
     * @param enid 企业管理主键
     * @return 结果
     */
    public int deleteEnterpriseByEnid(Long enid);
}
