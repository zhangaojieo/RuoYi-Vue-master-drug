package com.ruoyi.jy.service;

import java.util.List;
import com.ruoyi.jy.domain.Province;

/**
 * 省会管理Service接口
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
public interface IProvinceService
{
    /**
     * 查询省会管理
     *
     * @param proid 省会管理主键
     * @return 省会管理
     */
    public Province selectProvinceByProid(Long proid);

    /**
     * 查询省会管理列表
     *
     * @param province 省会管理
     * @return 省会管理集合
     */
    public List<Province> selectProvinceList(Province province);

    /**
     * 新增省会管理
     *
     * @param province 省会管理
     * @return 结果
     */
    public int insertProvince(Province province);

    /**
     * 修改省会管理
     *
     * @param province 省会管理
     * @return 结果
     */
    public int updateProvince(Province province);

    /**
     * 批量删除省会管理
     *
     * @param proids 需要删除的省会管理主键集合
     * @return 结果
     */
    public int deleteProvinceByProids(Long[] proids);

    /**
     * 删除省会管理信息
     *
     * @param proid 省会管理主键
     * @return 结果
     */
    public int deleteProvinceByProid(Long proid);

    /**
     * 查询所有省份信息
     * @return
     */
    List<Province> getProvinces();
}
