package com.ruoyi.jy.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.SpecialityMapper;
import com.ruoyi.jy.domain.Speciality;
import com.ruoyi.jy.service.ISpecialityService;

/**
 * 专业管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Service
public class SpecialityServiceImpl implements ISpecialityService
{
    @Autowired
    private SpecialityMapper specialityMapper;

    /**
     * 查询专业管理
     *
     * @param spid 专业管理主键
     * @return 专业管理
     */
    @Override
    public Speciality selectSpecialityBySpid(Long spid)
    {
        return specialityMapper.selectSpecialityBySpid(spid);
    }

    /**
     * 查询专业管理列表
     *
     * @param speciality 专业管理
     * @return 专业管理
     */
    @Override
    public List<Speciality> selectSpecialityList(Speciality speciality)
    {
        return specialityMapper.selectSpecialityList(speciality);
    }

    /**
     * 新增专业管理
     *
     * @param speciality 专业管理
     * @return 结果
     */
    @Override
    public int insertSpeciality(Speciality speciality)
    {
        speciality.setCreatetime(DateUtils.getTime());
        return specialityMapper.insertSpeciality(speciality);
    }

    /**
     * 修改专业管理
     *
     * @param speciality 专业管理
     * @return 结果
     */
    @Override
    public int updateSpeciality(Speciality speciality)
    {
        speciality.setModifytime(DateUtils.getTime());
        return specialityMapper.updateSpeciality(speciality);
    }

    /**
     * 批量删除专业管理
     *
     * @param spids 需要删除的专业管理主键
     * @return 结果
     */
    @Override
    public int deleteSpecialityBySpids(Long[] spids)
    {
        return specialityMapper.deleteSpecialityBySpids(spids);
    }

    /**
     * 删除专业管理信息
     *
     * @param spid 专业管理主键
     * @return 结果
     */
    @Override
    public int deleteSpecialityBySpid(Long spid)
    {
        return specialityMapper.deleteSpecialityBySpid(spid);
    }
}
