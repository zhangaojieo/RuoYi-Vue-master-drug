package com.ruoyi.jy.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.StudentemploymentMapper;
import com.ruoyi.jy.domain.Studentemployment;
import com.ruoyi.jy.service.IStudentemploymentService;

/**
 * 就业管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Service
public class StudentemploymentServiceImpl implements IStudentemploymentService
{
    @Autowired
    private StudentemploymentMapper studentemploymentMapper;

    @Override
    public List<Studentemployment> getSpeciStuEmpCount() {
        return studentemploymentMapper.getSpeciStuEmpCount();
    }

    @Override
    public List<Studentemployment> getSpeciAvgMoney() {
        return studentemploymentMapper.getSpeciAvgMoney();
    }

    @Override
    public List<Studentemployment> getStuEmpCityTotal() {
        return studentemploymentMapper.getStuEmpCityTotal();
    }

    @Override
    public int getStudentEmpUnTotal() {
        return studentemploymentMapper.getStudentEmpUnTotal();
    }

    @Override
    public int getStudentEmpTotal() {
        return studentemploymentMapper.getStudentEmpTotal();
    }

    @Override
    public int getFinishCollegeStuTotal() {
        return studentemploymentMapper.getFinishCollegeStuTotal();
    }

    /**
     * 查询就业管理
     *
     * @param sid 就业管理主键
     * @return 就业管理
     */
    @Override
    public Studentemployment selectStudentemploymentBySid(Long sid)
    {
        return studentemploymentMapper.selectStudentemploymentBySid(sid);
    }

    /**
     * 查询就业管理列表
     *
     * @param studentemployment 就业管理
     * @return 就业管理
     */
    @Override
    public List<Studentemployment> selectStudentemploymentList(Studentemployment studentemployment)
    {
        return studentemploymentMapper.selectStudentemploymentList(studentemployment);
    }

    /**
     * 新增就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    @Override
    public int insertStudentemployment(Studentemployment studentemployment)
    {
        return studentemploymentMapper.insertStudentemployment(studentemployment);
    }

    /**
     * 修改就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    @Override
    public int updateStudentemployment(Studentemployment studentemployment)
    {
        return studentemploymentMapper.updateStudentemployment(studentemployment);
    }

    /**
     * 批量删除就业管理
     *
     * @param sids 需要删除的就业管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentemploymentBySids(Long[] sids)
    {
        return studentemploymentMapper.deleteStudentemploymentBySids(sids);
    }

    /**
     * 删除就业管理信息
     *
     * @param sid 就业管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentemploymentBySid(Long sid)
    {
        return studentemploymentMapper.deleteStudentemploymentBySid(sid);
    }
}
