package com.ruoyi.jy.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.ProvinceMapper;
import com.ruoyi.jy.domain.Province;
import com.ruoyi.jy.service.IProvinceService;

/**
 * 省会管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Service
public class ProvinceServiceImpl implements IProvinceService
{
    @Autowired
    private ProvinceMapper provinceMapper;

    /**
     * 查询省会管理
     *
     * @param proid 省会管理主键
     * @return 省会管理
     */
    @Override
    public Province selectProvinceByProid(Long proid)
    {
        return provinceMapper.selectProvinceByProid(proid);
    }

    /**
     * 查询省会管理列表
     *
     * @param province 省会管理
     * @return 省会管理
     */
    @Override
    public List<Province> selectProvinceList(Province province)
    {
        return provinceMapper.selectProvinceList(province);
    }

    /**
     * 新增省会管理
     *
     * @param province 省会管理
     * @return 结果
     */
    @Override
    public int insertProvince(Province province)
    {
        province.setCreatetime(DateUtils.getTime());
        return provinceMapper.insertProvince(province);
    }

    /**
     * 修改省会管理
     *
     * @param province 省会管理
     * @return 结果
     */
    @Override
    public int updateProvince(Province province)
    {
        province.setModifytime(DateUtils.getTime());
        return provinceMapper.updateProvince(province);
    }

    /**
     * 批量删除省会管理
     *
     * @param proids 需要删除的省会管理主键
     * @return 结果
     */
    @Override
    public int deleteProvinceByProids(Long[] proids)
    {
        return provinceMapper.deleteProvinceByProids(proids);
    }

    /**
     * 删除省会管理信息
     *
     * @param proid 省会管理主键
     * @return 结果
     */
    @Override
    public int deleteProvinceByProid(Long proid)
    {
        return provinceMapper.deleteProvinceByProid(proid);
    }

    @Override
    public List<Province> getProvinces() {
        return provinceMapper.getProvinces();
    }
}
