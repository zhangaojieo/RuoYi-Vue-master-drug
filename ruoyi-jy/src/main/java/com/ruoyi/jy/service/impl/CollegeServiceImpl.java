package com.ruoyi.jy.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jy.mapper.CollegeMapper;
import com.ruoyi.jy.domain.College;
import com.ruoyi.jy.service.ICollegeService;

/**
 * 学院管理Service业务层处理
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Service
public class CollegeServiceImpl implements ICollegeService
{
    @Autowired
    private CollegeMapper collegeMapper;

    /**
     * 查询学院管理
     *
     * @param coid 学院管理主键
     * @return 学院管理
     */
    @Override
    public College selectCollegeByCoid(Long coid)
    {
        return collegeMapper.selectCollegeByCoid(coid);
    }

    /**
     * 查询学院管理列表
     *
     * @param college 学院管理
     * @return 学院管理
     */
    @Override
    public List<College> selectCollegeList(College college)
    {
        return collegeMapper.selectCollegeList(college);
    }

    /**
     * 新增学院管理
     *
     * @param college 学院管理
     * @return 结果
     */
    @Override
    public int insertCollege(College college)
    {
        college.setCreatetime(DateUtils.getTime());
        return collegeMapper.insertCollege(college);
    }

    /**
     * 修改学院管理
     *
     * @param college 学院管理
     * @return 结果
     */
    @Override
    public int updateCollege(College college)
    {
        college.setModifytime(DateUtils.getTime());
        return collegeMapper.updateCollege(college);
    }

    /**
     * 批量删除学院管理
     *
     * @param coids 需要删除的学院管理主键
     * @return 结果
     */
    @Override
    public int deleteCollegeByCoids(Long[] coids)
    {
        return collegeMapper.deleteCollegeByCoids(coids);
    }

    /**
     * 删除学院管理信息
     *
     * @param coid 学院管理主键
     * @return 结果
     */
    @Override
    public int deleteCollegeByCoid(Long coid)
    {
        return collegeMapper.deleteCollegeByCoid(coid);
    }

    @Override
    public List<College> getAllColleges() {
        return collegeMapper.getAllColleges();
    }
}
