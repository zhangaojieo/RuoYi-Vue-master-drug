package com.ruoyi.jy.mapper;

import java.util.List;
import com.ruoyi.jy.domain.College;
import org.springframework.stereotype.Repository;

/**
 * 学院管理Mapper接口
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Repository
public interface CollegeMapper
{
    /**
     * 查询学院管理
     *
     * @param coid 学院管理主键
     * @return 学院管理
     */
    public College selectCollegeByCoid(Long coid);

    /**
     * 查询学院管理列表
     *
     * @param college 学院管理
     * @return 学院管理集合
     */
    public List<College> selectCollegeList(College college);

    /**
     * 新增学院管理
     *
     * @param college 学院管理
     * @return 结果
     */
    public int insertCollege(College college);

    /**
     * 修改学院管理
     *
     * @param college 学院管理
     * @return 结果
     */
    public int updateCollege(College college);

    /**
     * 删除学院管理
     *
     * @param coid 学院管理主键
     * @return 结果
     */
    public int deleteCollegeByCoid(Long coid);

    /**
     * 批量删除学院管理
     *
     * @param coids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCollegeByCoids(Long[] coids);

    List<College>getAllColleges();
}
