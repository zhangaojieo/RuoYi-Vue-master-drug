package com.ruoyi.jy.mapper;

import java.util.List;
import com.ruoyi.jy.domain.Studentemployment;
import org.springframework.stereotype.Repository;

/**
 * 就业管理Mapper接口
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Repository
public interface StudentemploymentMapper
{

    /**
     * 统计已就业人数
     * @return
     */
    int getStudentEmpTotal();

    /**
     * 统计未就业人数
     * @return
     */
    int getStudentEmpUnTotal();

    /**
     * 统计毕业人数
     * @return
     */
    int getFinishCollegeStuTotal();

    /**
     * 统计分析不同专业就业人数
     * @return
     */
    List<Studentemployment>getSpeciStuEmpCount();

    /**
     * 统计不同专业的就业薪资
     * @return
     */
    List<Studentemployment>getSpeciAvgMoney();

    /**
     * 统计不同城市就业人数
     * @return
     */
    List<Studentemployment> getStuEmpCityTotal();

    /**
     * 查询就业管理
     *
     * @param sid 就业管理主键
     * @return 就业管理
     */
    public Studentemployment selectStudentemploymentBySid(Long sid);

    /**
     * 查询就业管理列表
     *
     * @param studentemployment 就业管理
     * @return 就业管理集合
     */
    public List<Studentemployment> selectStudentemploymentList(Studentemployment studentemployment);

    /**
     * 新增就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    public int insertStudentemployment(Studentemployment studentemployment);

    /**
     * 修改就业管理
     *
     * @param studentemployment 就业管理
     * @return 结果
     */
    public int updateStudentemployment(Studentemployment studentemployment);

    /**
     * 删除就业管理
     *
     * @param sid 就业管理主键
     * @return 结果
     */
    public int deleteStudentemploymentBySid(Long sid);

    /**
     * 批量删除就业管理
     *
     * @param sids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentemploymentBySids(Long[] sids);
}
