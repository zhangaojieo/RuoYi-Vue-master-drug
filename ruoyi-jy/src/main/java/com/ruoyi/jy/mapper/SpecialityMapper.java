package com.ruoyi.jy.mapper;

import java.util.List;
import com.ruoyi.jy.domain.Speciality;
import org.springframework.stereotype.Repository;

/**
 * 专业管理Mapper接口
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Repository
public interface SpecialityMapper
{
    /**
     * 查询专业管理
     *
     * @param spid 专业管理主键
     * @return 专业管理
     */
    public Speciality selectSpecialityBySpid(Long spid);

    /**
     * 查询专业管理列表
     *
     * @param speciality 专业管理
     * @return 专业管理集合
     */
    public List<Speciality> selectSpecialityList(Speciality speciality);

    /**
     * 新增专业管理
     *
     * @param speciality 专业管理
     * @return 结果
     */
    public int insertSpeciality(Speciality speciality);

    /**
     * 修改专业管理
     *
     * @param speciality 专业管理
     * @return 结果
     */
    public int updateSpeciality(Speciality speciality);

    /**
     * 删除专业管理
     *
     * @param spid 专业管理主键
     * @return 结果
     */
    public int deleteSpecialityBySpid(Long spid);

    /**
     * 批量删除专业管理
     *
     * @param spids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSpecialityBySpids(Long[] spids);
}
