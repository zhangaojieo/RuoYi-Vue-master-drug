package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 城市管理对象 city
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class City implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 城市id */
    private Long cityid;

    /** 所在省 */
    @Excel(name = "所在省")
    private Long proid;
    //查询显示省份名称
    private String proname;

    /** 城市编号 */
    @Excel(name = "城市编号")
    private String citynum;

    /** 城市名称 */
    @Excel(name = "城市名称")
    private String cityname;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 删除标记0未删除1已删除 */
    private Long deleted;

    @Excel(name = "创建时间")
    private String createtime;

}
