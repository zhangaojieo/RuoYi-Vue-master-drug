package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 学院管理对象 college
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class College implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 学校ID */
    private Long coid;

    /** 高校编号 */
    @Excel(name = "高校编号")
    private String conum;

    /** 高校名称 */
    @Excel(name = "高校名称")
    private String coname;

    /** 简介 */
    @Excel(name = "简介")
    private String introduce;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;
    @Excel(name = "创建时间")
    private String createtime;

    /** 删除标记 */
    private Long deleted;

    /** 操作人 */
    @Excel(name = "操作人")
    private String opername;
}
