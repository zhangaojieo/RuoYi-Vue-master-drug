package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 企业管理对象 enterprise
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Enterprise implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 企业ID */
    private Long enid;

    /** 企业编号 */
    @Excel(name = "企业编号")
    private String ennum;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String enname;

    /** 企业简介 */
    @Excel(name = "企业简介")
    private String introduction;

    /** 企业性质 */
    @Excel(name = "企业性质")
    private String nature;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 修改时间 */
    @Excel(name = "创建时间")
    private String createtime;
    @Excel(name = "备注")
    private String remark;

    /** 删除标记 */
    private Long deleted;

}
