package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 就业管理对象 studentemployment
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Studentemployment implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 学生ID */
    private Long sid;

    /** 城市 */
    @Excel(name = "城市")
    private Long cityid;
    private String cityname;
    private String totalCity; //显示统计就业城市人数
    private String name; //统计不同专业名称
    private String value; //统计就业人数值

    /** 专业 */
    @Excel(name = "专业")
    private Long spid;
    private String spname;

    /** 企业 */
    @Excel(name = "企业")
    private Long enid;
    private String enname;

    /** 学号 */
    @Excel(name = "学号")
    private String snum;

    /** 姓名 */
    @Excel(name = "姓名")
    private String sname;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 年龄 */
    @Excel(name = "年龄")
    private String age;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 微信 */
    @Excel(name = "微信")
    private String weixin;

    /** qq */
    @Excel(name = "qq")
    private String qq;

    /** 学历 */
    @Excel(name = "学历")
    private String education;

    /** 籍贯 */
    @Excel(name = "籍贯")
    private String nativeplace;

    /** 民族 */
    @Excel(name = "民族")
    private String nation;

    /** 出生日期 */
    @Excel(name = "出生日期")
    private String birthday;

    /** 入学时间 */
    @Excel(name = "入学时间")
    private String intime;

    /** 毕业时间 */
    @Excel(name = "毕业时间")
    private String outtime;

    /** 就业时间 */
    @Excel(name = "就业时间")
    private String etime;

    /** 就业薪资 */
    @Excel(name = "就业薪资")
    private String money;

    /** 就业状态 */
    @Excel(name = "就业状态")
    private Long stat;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    private String createtime;

    /** 删除标记 */
    private Long deleted;
}
