package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import java.io.Serializable;

/**
 * 专业管理对象 speciality
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Speciality implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 专业ID */
    private Long spid;

    /** 学院学院 */
    @Excel(name = "学院学院")
    private Long coid;
    /** 显示学院名称 */
    private String coname;

    /** 专业编号 */
    @Excel(name = "专业编号")
    private String spnum;

    /** 专业名称 */
    @Excel(name = "专业名称")
    private String spname;

    /** 专业简介 */
    @Excel(name = "专业简介")
    private String introduce;
    @Excel(name = "创建时间")
    private String createtime;
    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    /** 删除标记 */
    private Long deleted;

}
