package com.ruoyi.jy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 省会管理对象 province
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Province implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 省ID */
    private Long proid;

    /** 省编号 */
    @Excel(name = "省编号")
    private String pronum;

    /** 省名称 */
    @Excel(name = "省名称")
    private String proname;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private String modifytime;

    @Excel(name = "添加时间")
    private String createtime;

    /** 删除标记 */
    @Excel(name = "删除标记")
    private Long deleted;

}
