package com.ruoyi.jy.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jy.domain.City;
import com.ruoyi.jy.service.ICityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 城市管理Controller
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@RestController
@RequestMapping("/city/city")
public class CityController extends BaseController
{
    @Autowired
    private ICityService cityService;

    /**
     * 查询城市管理列表
     */
    @PreAuthorize("@ss.hasPermi('city:city:list')")
    @GetMapping("/list")
    public TableDataInfo list(City city)
    {
        startPage();
        List<City> list = cityService.selectCityList(city);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('city:city:getAllCityByProvinceId')")
    @GetMapping("/getAllCityByProvinceId/{proid}")
    public AjaxResult getAllCityByProvinceId(@PathVariable("proid")int proid){
        return success(cityService.getAllCityByProviceId(proid));
    }

    /**
     * 导出城市管理列表
     */
    @PreAuthorize("@ss.hasPermi('city:city:export')")
    @Log(title = "城市管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, City city)
    {
        List<City> list = cityService.selectCityList(city);
        ExcelUtil<City> util = new ExcelUtil<City>(City.class);
        util.exportExcel(response, list, "城市管理数据");
    }

    /**
     * 获取城市管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('city:city:query')")
    @GetMapping(value = "/{cityid}")
    public AjaxResult getInfo(@PathVariable("cityid") Long cityid)
    {
        return success(cityService.selectCityByCityid(cityid));
    }

    /**
     * 新增城市管理
     */
    @PreAuthorize("@ss.hasPermi('city:city:add')")
    @Log(title = "城市管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody City city)
    {
        return toAjax(cityService.insertCity(city));
    }

    /**
     * 修改城市管理
     */
    @PreAuthorize("@ss.hasPermi('city:city:edit')")
    @Log(title = "城市管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody City city)
    {
        return toAjax(cityService.updateCity(city));
    }

    /**
     * 删除城市管理
     */
    @PreAuthorize("@ss.hasPermi('city:city:remove')")
    @Log(title = "城市管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cityids}")
    public AjaxResult remove(@PathVariable Long[] cityids)
    {
        return toAjax(cityService.deleteCityByCityids(cityids));
    }
}
