package com.ruoyi.jy.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jy.domain.Studentemployment;
import com.ruoyi.jy.service.IStudentemploymentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 就业管理Controller
 *
 * @author 张澳杰
 * @date 2023-07-28
 */
@RestController
@RequestMapping("/studentemployment/studentemployment")
public class StudentemploymentController extends BaseController
{
    @Autowired
    private IStudentemploymentService studentemploymentService;

    /**
     * 统计已就业人数
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getStudentEmpTotal')")
    @GetMapping("/getStudentEmpTotal")
    public AjaxResult getStudentEmpTotal(){
        return success(studentemploymentService.getStudentEmpTotal());
    }

    /**
     * 统计未就业人数
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getStudentEmpUnTotal')")
    @GetMapping("/getStudentEmpUnTotal")
    public AjaxResult getStudentEmpUnTotal(){
        return success(studentemploymentService.getStudentEmpUnTotal());
    }

    /**
     * 统计毕业人数
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getFinishCollegeStuTotal')")
    @GetMapping("/getFinishCollegeStuTotal")
    public AjaxResult getFinishCollegeStuTotal(){
        return success(studentemploymentService.getFinishCollegeStuTotal());
    }

    /**
     * 统计不同专业的就业薪资
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getSpeciAvgMoney')")
    @GetMapping("/getSpeciAvgMoney")
    public AjaxResult getSpeciAvgMoney(){
        return success(studentemploymentService.getSpeciAvgMoney());
    }

    /**
     * 统计不同专业的就业人数
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getSpeciStuEmpCount')")
    @GetMapping("/getSpeciStuEmpCount")
    public AjaxResult getSpeciStuEmpCount(){
        return success(studentemploymentService.getSpeciStuEmpCount());
    }

    /**
     * 统计各个城市的就业人数
     * @return
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:getStuEmpCityTotal')")
    @GetMapping("/getStuEmpCityTotal")
    public AjaxResult getStuEmpCityTotal(){
        return success(studentemploymentService.getStuEmpCityTotal());
    }

    /**
     * 查询就业管理列表
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:list')")
    @GetMapping("/list")
    public TableDataInfo list(Studentemployment studentemployment)
    {
        startPage();
        List<Studentemployment> list = studentemploymentService.selectStudentemploymentList(studentemployment);
        return getDataTable(list);
    }

    /**
     * 导出就业管理列表
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:export')")
    @Log(title = "就业管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Studentemployment studentemployment)
    {
        List<Studentemployment> list = studentemploymentService.selectStudentemploymentList(studentemployment);
        ExcelUtil<Studentemployment> util = new ExcelUtil<Studentemployment>(Studentemployment.class);
        util.exportExcel(response, list, "就业管理数据");
    }

    /**
     * 获取就业管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:query')")
    @GetMapping(value = "/{sid}")
    public AjaxResult getInfo(@PathVariable("sid") Long sid)
    {
        return success(studentemploymentService.selectStudentemploymentBySid(sid));
    }

    /**
     * 新增就业管理
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:add')")
    @Log(title = "就业管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Studentemployment studentemployment)
    {
        return toAjax(studentemploymentService.insertStudentemployment(studentemployment));
    }

    /**
     * 修改就业管理
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:edit')")
    @Log(title = "就业管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Studentemployment studentemployment)
    {
        return toAjax(studentemploymentService.updateStudentemployment(studentemployment));
    }

    /**
     * 删除就业管理
     */
    @PreAuthorize("@ss.hasPermi('studentemployment:studentemployment:remove')")
    @Log(title = "就业管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{sids}")
    public AjaxResult remove(@PathVariable Long[] sids)
    {
        return toAjax(studentemploymentService.deleteStudentemploymentBySids(sids));
    }
}
