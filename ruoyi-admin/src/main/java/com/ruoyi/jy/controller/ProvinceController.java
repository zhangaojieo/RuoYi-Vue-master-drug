package com.ruoyi.jy.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jy.domain.Province;
import com.ruoyi.jy.service.IProvinceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 省会管理Controller
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@RestController
@RequestMapping("/province/province")
public class ProvinceController extends BaseController
{
    @Autowired
    private IProvinceService provinceService;

    /**
     * 查询省会管理列表
     */
    @PreAuthorize("@ss.hasPermi('province:province:list')")
    @GetMapping("/list")
    public TableDataInfo list(Province province)
    {
        startPage();
        List<Province> list = provinceService.selectProvinceList(province);
        return getDataTable(list);
    }

    /**
     * 导出省会管理列表
     */
    @PreAuthorize("@ss.hasPermi('province:province:export')")
    @Log(title = "省会管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Province province)
    {
        List<Province> list = provinceService.selectProvinceList(province);
        ExcelUtil<Province> util = new ExcelUtil<Province>(Province.class);
        util.exportExcel(response, list, "省会管理数据");
    }

    /**
     * 获取省会管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('province:province:query')")
    @GetMapping(value = "/{proid}")
    public AjaxResult getInfo(@PathVariable("proid") Long proid)
    {
        return success(provinceService.selectProvinceByProid(proid));
    }

    /**
     * 查询所有省份
     * @return
     */
    @PreAuthorize("@ss.hasPermi('province:province:getProvinces')")
    @GetMapping(value = "/getProvinces")
    public AjaxResult getProvinces()
    {
        return success(provinceService.getProvinces());
    }

    /**
     * 新增省会管理
     */
    @PreAuthorize("@ss.hasPermi('province:province:add')")
    @Log(title = "省会管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Province province)
    {
        return super.toAjax(provinceService.insertProvince(province));
    }

    /**
     * 修改省会管理
     */
    @PreAuthorize("@ss.hasPermi('province:province:edit')")
    @Log(title = "省会管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Province province)
    {
        return toAjax(provinceService.updateProvince(province));
    }

    /**
     * 删除省会管理
     */
    @PreAuthorize("@ss.hasPermi('province:province:remove')")
    @Log(title = "省会管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{proids}")
    public AjaxResult remove(@PathVariable Long[] proids)
    {
        return toAjax(provinceService.deleteProvinceByProids(proids));
    }
}
