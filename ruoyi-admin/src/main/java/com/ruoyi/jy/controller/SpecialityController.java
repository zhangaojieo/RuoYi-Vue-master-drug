package com.ruoyi.jy.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jy.domain.Speciality;
import com.ruoyi.jy.service.ISpecialityService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 专业管理Controller
 * 
 * @author 张澳杰
 * @date 2023-07-28
 */
@RestController
@RequestMapping("/speciality/speciality")
public class SpecialityController extends BaseController
{
    @Autowired
    private ISpecialityService specialityService;

    /**
     * 查询专业管理列表
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:list')")
    @GetMapping("/list")
    public TableDataInfo list(Speciality speciality)
    {
        startPage();
        List<Speciality> list = specialityService.selectSpecialityList(speciality);
        return getDataTable(list);
    }

    /**
     * 导出专业管理列表
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:export')")
    @Log(title = "专业管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Speciality speciality)
    {
        List<Speciality> list = specialityService.selectSpecialityList(speciality);
        ExcelUtil<Speciality> util = new ExcelUtil<Speciality>(Speciality.class);
        util.exportExcel(response, list, "专业管理数据");
    }

    /**
     * 获取专业管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:query')")
    @GetMapping(value = "/{spid}")
    public AjaxResult getInfo(@PathVariable("spid") Long spid)
    {
        return success(specialityService.selectSpecialityBySpid(spid));
    }

    /**
     * 新增专业管理
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:add')")
    @Log(title = "专业管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Speciality speciality)
    {
        return toAjax(specialityService.insertSpeciality(speciality));
    }

    /**
     * 修改专业管理
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:edit')")
    @Log(title = "专业管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Speciality speciality)
    {
        return toAjax(specialityService.updateSpeciality(speciality));
    }

    /**
     * 删除专业管理
     */
    @PreAuthorize("@ss.hasPermi('speciality:speciality:remove')")
    @Log(title = "专业管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{spids}")
    public AjaxResult remove(@PathVariable Long[] spids)
    {
        return toAjax(specialityService.deleteSpecialityBySpids(spids));
    }
}
