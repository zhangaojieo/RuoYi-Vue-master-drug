package com.ruoyi.jy.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jy.domain.College;
import com.ruoyi.jy.service.ICollegeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学院管理Controller
 *
 * @author 张澳杰
 * @date 2023-07-27
 */
@RestController
@RequestMapping("/college/college")
public class CollegeController extends BaseController
{
    @Autowired
    private ICollegeService collegeService;

    /**
     * 查询学院管理列表
     */
    @PreAuthorize("@ss.hasPermi('college:college:list')")
    @GetMapping("/list")
    public TableDataInfo list(College college)
    {
        startPage();
        List<College> list = collegeService.selectCollegeList(college);
        return getDataTable(list);
    }

    /**
     * 导出学院管理列表
     */
    @PreAuthorize("@ss.hasPermi('college:college:export')")
    @Log(title = "学院管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, College college)
    {
        List<College> list = collegeService.selectCollegeList(college);
        ExcelUtil<College> util = new ExcelUtil<College>(College.class);
        util.exportExcel(response, list, "学院管理数据");
    }

    /**
     * 获取学院管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('college:college:query')")
    @GetMapping(value = "/{coid}")
    public AjaxResult getInfo(@PathVariable("coid") Long coid)
    {
        return success(collegeService.selectCollegeByCoid(coid));
    }

    /**
     * 查询所有学院信息
     * @return
     */
    @PreAuthorize("@ss.hasPermi('college:college:getAllColleges')")
    @GetMapping(value = "/getAllColleges")
    public AjaxResult getAllColleges()
    {
        return success(collegeService.getAllColleges());
    }

    /**
     * 新增学院管理
     */
    @PreAuthorize("@ss.hasPermi('college:college:add')")
    @Log(title = "学院管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody College college)
    {
        college.setOpername(super.getUsername());
        return toAjax(collegeService.insertCollege(college));
    }

    /**
     * 修改学院管理
     */
    @PreAuthorize("@ss.hasPermi('college:college:edit')")
    @Log(title = "学院管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody College college)
    {
        college.setOpername(super.getUsername());
        return toAjax(collegeService.updateCollege(college));
    }

    /**
     * 删除学院管理
     */
    @PreAuthorize("@ss.hasPermi('college:college:remove')")
    @Log(title = "学院管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{coids}")
    public AjaxResult remove(@PathVariable Long[] coids)
    {
        return toAjax(collegeService.deleteCollegeByCoids(coids));
    }
}
