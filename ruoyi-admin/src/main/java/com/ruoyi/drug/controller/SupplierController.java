package com.ruoyi.drug.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.drug.domain.Store;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Supplier;
import com.ruoyi.drug.service.ISupplierService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 供应商管理Controller
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/supplier/supplier")
public class SupplierController extends BaseController {
    @Autowired
    private ISupplierService supplierService;

    /**
     * 查询供应商管理列表
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:list')")
    @GetMapping("/list")
    public TableDataInfo list(Supplier supplier) {
        startPage();
        QueryWrapper<Supplier> queryWrapper = new QueryWrapper();
        if (supplier.getSupname() != null && !"".equals(supplier.getSupname())) {
            queryWrapper.like("supname", supplier.getSupname());
        }
        List<Supplier> list = supplierService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出供应商管理列表
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:export')")
    @Log(title = "供应商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Supplier supplier) {
        QueryWrapper<Supplier> queryWrapper = new QueryWrapper();
        if (supplier.getSupname() != null && !"".equals(supplier.getSupname())) {
            queryWrapper.like("supname", supplier.getSupname());
        }
        List<Supplier> list = supplierService.list(queryWrapper);
        ExcelUtil<Supplier> util = new ExcelUtil<Supplier>(Supplier.class);
        util.exportExcel(response, list, "供应商管理数据");
    }

    /**
     * 获取供应商管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:query')")
    @GetMapping(value = "/{supid}")
    public AjaxResult getInfo(@PathVariable("supid") Long supid) {
        return success(supplierService.getById(supid));
    }

    /**
     * 新增供应商管理
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:add')")
    @Log(title = "供应商管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Supplier supplier) {
        supplier.setCreatetime(DateUtils.getTime());
        return toAjax(supplierService.save(supplier));
    }

    /**
     * 修改供应商管理
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:edit')")
    @Log(title = "供应商管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Supplier supplier) {
        supplier.setModifytime(DateUtils.getDate());
        return toAjax(supplierService.updateById(supplier));
    }

    /**
     * 查询所有供应商信息
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:getAllSuppliers')")
    @GetMapping("/getAllSuppliers")
    public AjaxResult getAllSuppliers() {
        return success(supplierService.list());
    }

    /**
     * 删除供应商管理
     */
    @PreAuthorize("@ss.hasPermi('supplier:supplier:remove')")
    @Log(title = "供应商管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{supids}")
    public AjaxResult remove(@PathVariable List<Long> supids) {
        return toAjax(supplierService.removeByIds(supids));
    }
}
