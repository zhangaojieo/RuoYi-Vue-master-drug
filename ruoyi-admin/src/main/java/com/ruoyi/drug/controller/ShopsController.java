package com.ruoyi.drug.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.drug.domain.Category;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Shops;
import com.ruoyi.drug.service.IShopsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 门店管理Controller
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/shops/shops")
public class ShopsController extends BaseController
{
    @Autowired
    private IShopsService shopsService;

    /**
     * 查询门店管理列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:list')")
    @GetMapping("/list")
    public TableDataInfo list(Shops shops)
    {
        startPage();
        QueryWrapper<Shops> queryWrapper = new QueryWrapper();
        if(shops.getSnum() !=null && !"".equals(shops.getSnum())){
            queryWrapper.like("snum", shops.getSnum());
        }
        if(shops.getCity() !=null && !"".equals(shops.getCity())){
            queryWrapper.like("city", shops.getCity());
        }
        if(shops.getSname() !=null && !"".equals(shops.getSname())){
            queryWrapper.like("sname", shops.getSname());
        }
        List<Shops> list = shopsService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出门店管理列表
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:export')")
    @Log(title = "门店管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Shops shops)
    {
        QueryWrapper<Shops> queryWrapper = new QueryWrapper();
        if(shops.getSnum() !=null && !"".equals(shops.getSnum())){
            queryWrapper.like("snum", shops.getSnum());
        }
        if(shops.getCity() !=null && !"".equals(shops.getCity())){
            queryWrapper.like("city", shops.getCity());
        }
        if(shops.getSname() !=null && !"".equals(shops.getSname())){
            queryWrapper.like("sname", shops.getSname());
        }
        List<Shops> list = shopsService.list(queryWrapper);
        ExcelUtil<Shops> util = new ExcelUtil<Shops>(Shops.class);
        util.exportExcel(response, list, "门店管理数据");
    }

    /**
     * 获取门店管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:query')")
    @GetMapping(value = "/{sid}")
    public AjaxResult getInfo(@PathVariable("sid") Long sid)
    {
        return success(shopsService.getById(sid));
    }

    /**
     * 新增门店管理
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:add')")
    @Log(title = "门店管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Shops shops)
    {
        shops.setCreatetime(DateUtils.getTime());
        return toAjax(shopsService.save(shops));
    }

    /**
     * 修改门店管理
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:edit')")
    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Shops shops)
    {
        shops.setModifytime(DateUtils.getTime());
        return toAjax(shopsService.updateById(shops));
    }

    /**
     * 删除门店管理
     */
    @PreAuthorize("@ss.hasPermi('shops:shops:remove')")
    @Log(title = "门店管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sids}")
    public AjaxResult remove(@PathVariable List<Long> sids)
    {
        return toAjax(shopsService.removeByIds(sids));
    }
}
