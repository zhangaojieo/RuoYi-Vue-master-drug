package com.ruoyi.drug.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.service.ISysUserService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Brand;
import com.ruoyi.drug.service.IBrandService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 品牌管理Controller
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/brand/brand")
public class BrandController extends BaseController {
    @Autowired
    private IBrandService brandService;

    /**
     * 查询品牌管理列表
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:list')")
    @GetMapping("/list")
    public TableDataInfo list(Brand brand) {
        startPage();
        QueryWrapper<Brand> queryWrapper = new QueryWrapper<>();
        if (brand.getBrname() != null && !"".equals(brand.getBrname())) {
            queryWrapper.like("BRNAME", brand.getBrname());
        }
        List<Brand> list = brandService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 查询所有品牌信息
     * @return
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:getAllBrands')")
    @GetMapping("/getAllBrands")
    public AjaxResult getAllBrands(){
        return success(brandService.list());
    }

    /**
     * 导出品牌管理列表
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:export')")
    @Log(title = "品牌管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Brand brand) {
        QueryWrapper<Brand> queryWrapper = new QueryWrapper<>();
        if (brand.getBrname() != null && !"".equals(brand.getBrname())) {
            queryWrapper.like("BRNAME", brand.getBrname());
        }
        List<Brand> list = brandService.list(queryWrapper);
        ExcelUtil<Brand> util = new ExcelUtil<Brand>(Brand.class);
        util.exportExcel(response, list, "品牌管理数据");
    }

    /**
     * 获取品牌管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:query')")
    @GetMapping(value = "/{brid}")
    public AjaxResult getInfo(@PathVariable("brid") Long brid) {
        return success(brandService.getById(brid));
    }

    /**
     * 新增品牌管理
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:add')")
    @Log(title = "品牌管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Brand brand) {
        brand.setCreatetime(DateUtils.getTime());
        return toAjax(brandService.save(brand));
    }

    /**
     * 修改品牌管理
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:edit')")
    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Brand brand) {
        brand.setModifytime(DateUtils.getTime());
        return toAjax(brandService.updateById(brand));
    }

    /**
     * 删除品牌管理
     */
    @PreAuthorize("@ss.hasPermi('brand:brand:remove')")
    @Log(title = "品牌管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{brids}")
    public AjaxResult remove(@PathVariable List<Integer> brids) {
        return toAjax(brandService.removeByIds(brids));
    }
}
