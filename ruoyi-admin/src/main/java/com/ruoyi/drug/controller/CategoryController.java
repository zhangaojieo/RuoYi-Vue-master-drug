package com.ruoyi.drug.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Category;
import com.ruoyi.drug.service.ICategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 药品分类Controller
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/category/category")
public class CategoryController extends BaseController
{
    @Autowired
    private ICategoryService categoryService;

    /**
     * 查询所有药品类别
     * @return
     */
    @PreAuthorize("@ss.hasPermi('category:category:getAllCategory')")
    @GetMapping("/getAllCategory")
    public AjaxResult getAllCategory(){
        return success(categoryService.list());
    }

    /**
     * 查询药品分类列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(Category category)
    {
        startPage();
        QueryWrapper<Category> queryWrapper = new QueryWrapper();
        if(category.getCname() !=null && !"".equals(category.getCname())){
            queryWrapper.like("CNAME", category.getCname());
        }
        List<Category> list = categoryService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出药品分类列表
     */
    @PreAuthorize("@ss.hasPermi('category:category:export')")
    @Log(title = "药品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Category category)
    {
        QueryWrapper<Category> queryWrapper = new QueryWrapper();
        if(category.getCname() !=null && !"".equals(category.getCname())){
            queryWrapper.like("CNAME", category.getCname());
        }
        List<Category> list = categoryService.list(queryWrapper);
        ExcelUtil<Category> util = new ExcelUtil<Category>(Category.class);
        util.exportExcel(response, list, "药品分类数据");
    }

    /**
     * 获取药品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('category:category:query')")
    @GetMapping(value = "/{cid}")
    public AjaxResult getInfo(@PathVariable("cid") Long cid)
    {
        return success(categoryService.getById(cid));
    }

    /**
     * 新增药品分类
     */
    @PreAuthorize("@ss.hasPermi('category:category:add')")
    @Log(title = "药品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Category category)
    {
        category.setCreatetime(DateUtils.getTime());
        return toAjax(categoryService.save(category));
    }

    /**
     * 修改药品分类
     */
    @PreAuthorize("@ss.hasPermi('category:category:edit')")
    @Log(title = "药品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Category category)
    {
        category.setModifytime(DateUtils.getTime());
        return toAjax(categoryService.updateById(category));
    }

    /**
     * 删除药品分类
     */
    @PreAuthorize("@ss.hasPermi('category:category:remove')")
    @Log(title = "药品分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{cids}")
    public AjaxResult remove(@PathVariable List<Integer> cids)
    {
        return toAjax(categoryService.removeByIds(cids));
    }
}
