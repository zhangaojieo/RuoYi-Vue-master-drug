package com.ruoyi.drug.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.drug.domain.Category;
import com.ruoyi.drug.domain.Drug;
import com.ruoyi.drug.domain.Store;
import com.ruoyi.drug.service.IDrugService;
import com.ruoyi.drug.service.IStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author: ZhangAJ
 * @create: 2023年08月02日 14:06
 */
@RestController
@RequestMapping("/drug/drug")
public class DrugController extends BaseController {

    @Autowired
    private IDrugService drugService;
    @Autowired
    private IStoreService storeService;

    /**
     * 查询药品管理列表
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:list')")
    @GetMapping("/list")
    public TableDataInfo list(Drug drug) {
        startPage();
        List<Drug> list = drugService.list(drug);
        return getDataTable(list);
    }

    /**
     * 根据药品类别的ID查询所有药品
     *
     * @param cid
     * @return
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:getByCategoryIdAllDrug')")
    @GetMapping("/getByCategoryIdAllDrug/{cid}")
    public AjaxResult getByCategoryIdAllDrug(@PathVariable("cid") Long cid) {
        //药品类别]
        QueryWrapper<Drug> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cid", cid);
        return success(drugService.list(queryWrapper));
    }

    /**
     * 根据药ID查询所在仓库
     * @return
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:getDrugByIdStore')")
    @GetMapping("/getDrugByIdStore/{drugid}")
    public AjaxResult getDrugByIdStore(@PathVariable("drugid")Long drugid){
        Drug drug = drugService.getById(drugid);
        Store store = storeService.getById(drug.getStorid());
        return success(store);
    }

    /**
     * 查询所有药品
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:getAllDrugs')")
    @GetMapping("/getAllDrugs")
    public AjaxResult getAllDrugs() {
        return success(drugService.list());
    }

    /**
     * 导出药品管理列表
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:export')")
    @Log(title = "药品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Drug drug) {
        QueryWrapper<Drug> queryWrapper = new QueryWrapper();

        List<Drug> list = drugService.list(queryWrapper);
        ExcelUtil<Drug> util = new ExcelUtil<Drug>(Drug.class);
        util.exportExcel(response, list, "药品管理数据");
    }

    /**
     * 获取药品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:query')")
    @GetMapping(value = "/{drugid}")
    public AjaxResult getInfo(@PathVariable("drugid") Long drugid) {
        return success(drugService.getById(drugid));
    }

    /**
     * 新增药品管理
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:add')")
    @Log(title = "药品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Drug drug) {
        drug.setCreatetime(DateUtils.getTime());
        return toAjax(drugService.save(drug));
    }

    /**
     * 修改药品管理
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:edit')")
    @Log(title = "药品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Drug drug) {
        drug.setModifytime(DateUtils.getTime());
        return toAjax(drugService.updateById(drug));
    }

    /**
     * 删除药品管理
     */
    @PreAuthorize("@ss.hasPermi('drug:drug:remove')")
    @Log(title = "药品管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{drugids}")
    public AjaxResult remove(@PathVariable List<Long> drugids) {
        return toAjax(drugService.removeByIds(drugids));
    }
}
