package com.ruoyi.drug.controller;

import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Sell;
import com.ruoyi.drug.service.ISellService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 销售管理Controller
 *
 * @author 张澳杰
 * @date 2023-08-03
 */
@RestController
@RequestMapping("/sell/sell")
public class SellController extends BaseController
{
    @Autowired
    private ISellService sellService;

    /**
     * 查询销售管理列表
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sell sell)
    {
        startPage();
        List<Sell> list = sellService.list(sell);
        return getDataTable(list);
    }

    /**
     * 导出销售管理列表
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:export')")
    @Log(title = "销售管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Sell sell)
    {
        QueryWrapper<Sell> queryWrapper = new QueryWrapper<>();
        List<Sell> list = sellService.list(queryWrapper);
        ExcelUtil<Sell> util = new ExcelUtil<Sell>(Sell.class);
        util.exportExcel(response, list, "销售管理数据");
    }

    /**
     * 获取销售管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:query')")
    @GetMapping(value = "/{sellid}")
    public AjaxResult getInfo(@PathVariable("sellid") Long sellid)
    {
        return success(sellService.getById(sellid));
    }

    /**
     * 新增销售管理
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:add')")
    @Log(title = "销售管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sell sell)
    {
        sell.setOpename(getUsername());
        return toAjax(sellService.save(sell));
    }

    /**
     * 修改销售管理
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:edit')")
    @Log(title = "销售管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sell sell)
    {
        //随机生成销售单号
        sell.setSellnum(UUID.randomUUID().toString());
        sell.setOpename(getUsername());
        sell.setModifytime(DateUtils.dateTime());
        return toAjax(sellService.updateById(sell));
    }

    /**
     * 删除销售管理
     */
    @PreAuthorize("@ss.hasPermi('sell:sell:remove')")
    @Log(title = "销售管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sellids}")
    public AjaxResult remove(@PathVariable List<Long> sellids)
    {
        return toAjax(sellService.removeByIds(sellids));
    }
}
