package com.ruoyi.drug.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.drug.domain.Category;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.drug.domain.Store;
import com.ruoyi.drug.service.IStoreService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库管理Controller
 *
 * @author 张澳杰
 * @date 2023-08-01
 */
@RestController
@RequestMapping("/store/store")
public class StoreController extends BaseController
{
    @Autowired
    private IStoreService storeService;

    /**
     * 查询所有仓库信息
     * @return
     */
    @PreAuthorize("@ss.hasPermi('store:store:getAllStores')")
    @GetMapping("/getAllStores")
    public AjaxResult getAllStores(){
        return success(storeService.list());
    }

    /**
     * 查询仓库管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:store:list')")
    @GetMapping("/list")
    public TableDataInfo list(Store store)
    {
        startPage();
        QueryWrapper<Store> queryWrapper = new QueryWrapper();
        if(store.getStornum() !=null && !"".equals(store.getStornum())){
            queryWrapper.like("stornum", store.getStornum());
        }
        if(store.getStorename() !=null && !"".equals(store.getStorename())){
            queryWrapper.like("storename", store.getStorename());
        }
        if(store.getAddress() !=null && !"".equals(store.getAddress())){
            queryWrapper.like("address", store.getAddress());
        }
        List<Store> list = storeService.list(queryWrapper);
        return getDataTable(list);
    }

    /**
     * 导出仓库管理列表
     */
    @PreAuthorize("@ss.hasPermi('store:store:export')")
    @Log(title = "仓库管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Store store)
    {
        QueryWrapper<Store> queryWrapper = new QueryWrapper();
        if(store.getStornum() !=null && !"".equals(store.getStornum())){
            queryWrapper.like("stornum", store.getStornum());
        }
        if(store.getStorename() !=null && !"".equals(store.getStorename())){
            queryWrapper.like("storename", store.getStorename());
        }
        if(store.getAddress() !=null && !"".equals(store.getAddress())){
            queryWrapper.like("address", store.getAddress());
        }
        List<Store> list = storeService.list(queryWrapper);
        ExcelUtil<Store> util = new ExcelUtil<Store>(Store.class);
        util.exportExcel(response, list, "仓库管理数据");
    }

    /**
     * 获取仓库管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('store:store:query')")
    @GetMapping(value = "/{storid}")
    public AjaxResult getInfo(@PathVariable("storid") Long storid)
    {
        return success(storeService.getById(storid));
    }

    /**
     * 新增仓库管理
     */
    @PreAuthorize("@ss.hasPermi('store:store:add')")
    @Log(title = "仓库管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Store store)
    {
        store.setDirector(getUsername());
        store.setCreatetime(DateUtils.getTime());
        return toAjax(storeService.save(store));
    }

    /**
     * 修改仓库管理
     */
    @PreAuthorize("@ss.hasPermi('store:store:edit')")
    @Log(title = "仓库管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Store store)
    {
        store.setDirector(getUsername());
        store.setModifytime(DateUtils.getTime());
        return toAjax(storeService.updateById(store));
    }

    /**
     * 删除仓库管理
     */
    @PreAuthorize("@ss.hasPermi('store:store:remove')")
    @Log(title = "仓库管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{storids}")
    public AjaxResult remove(@PathVariable List<Long> storids)
    {
        return toAjax(storeService.removeByIds(storids));
    }
}
